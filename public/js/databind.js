// databind
// Rodrigo Nishino <rodrigo.nsh@gmail.com>

function databind()
{
  
  //console.log('databind');
  
  $body.querySelectorAll("[data-bind]").forEach(function(el, i)
  {
    
    try
    {

      var tag = el.tagName
      res = eval(el.dataset.bind)
      
      if ( undefined == res ) return
          
      if ( tag == "IMG" )
      {
        el.src = res
      }
      
      else if ( tag == "AUDIO" || tag == "VIDEO")
      {
        el.src = res
      }
      
      else if ( tag == "INPUT" || tag == "ONS-INPUT" )
      {
        el.value = res
      }
      
      else { el.textContent = res }
    
    }
    
    catch(err)
    {
      console.warn( err.message );
    }
    
  });
  
  $body.querySelectorAll('[bind-placeholder]').forEach( function(el, i)
  {
    let res = eval(el.getAttribute('bind-placeholder'))
    el.setAttribute('placeholder', res)
  });
  
}
