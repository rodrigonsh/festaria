

addEventListener("userReady", function()
{
 
  UID = user.uid
  
  console.log('userReady', UID);
  
  var setCustomClaims = firebase.functions().httpsCallable('setCustomClaims')
  
  setCustomClaims({'vendorUID': vendorUID})
    .then( function(res)
    {
      console.log("settingCustomClaims finished with", res)
      return res
    })
    .then( () => user.getIdToken(true) )
    .then( function(token)
    {

      //console.log("new token", token);
        
      if (uids.indexOf(vendorUID) == -1) uids.push(vendorUID);
      if (uids.indexOf(UID) == -1) uids.push(UID);
      
      refreshProfilesInfos();
      
      // get user public for vendor
      var path = ['users', UID, vendorUID, 'info'].join("/");
      db.ref(path).on('value', function(s)
      {
        
        var info = s.val();
        
        if ( info == null ) pageTo("profile-edit.html");
        else profiles[UID] = s.val();
            
        databind();
        
      })
      
      // get user personal for vendor
      var path = ['users', UID, vendorUID, 'personal'].join("/");
      db.ref(path).on('value', function(s)
      {
        personal[UID] = s.val();
        databind();
      })
      
      if( UID == vendorUID )
      {
        $body.setAttribute('is-admin', true);
      }
      
      //console.log('UIDS before club onvalue', uids);
    
     
      db.ref("clubs/"+vendorUID).on("value", function(s)
      {
        
        club = s.val();
        localStorage.setItem("club", JSON.stringify(club));
        emit("clubChanged");
        
        if( ! ( UID in profiles ) ) pageTo("profile-edit.html")
        else
        {
          
          console.warn(page);
          
          if (
            page.id == "startup.html" || 
            page.id == "user-auth.html" ||
            page.id == "user-new.html"
            )
          {
            pageTo("home.html");
          }
        }
        
      })
  
        
      })
      .catch(console.error)
  
  
  // todo: stalker mode faz um on value ao invés e pega os updates em
  // tempo real
  
  
  
})



addEventListener("noUser", function() { pageTo("user-auth.html") })
addEventListener("userAuth", function() { pageTo("user-auth.html") })
addEventListener("userNew", function() { pageTo("user-new.html") })

addEventListener('user-authInit', function()
{
  
  page = pages['user-auth.html']
  
  var  form = page.querySelector('form')
  
  console.log("form", form);
  
  form.addEventListener('submit', function(ev)
  {
    
    ev.preventDefault();
    ev.stopPropagation();
    
    form.setAttribute("loading", true);
    
    
    let e = form.querySelector("[type=email]").value;
    let p = form.querySelector("[type=password]").value;
    
    auth
      .signInWithEmailAndPassword(e, p)
      .catch(function(error)
      {
        ons.notification.alert(error.message);
        form.removeAttribute("loading");
        form.setAttribute("failed", true);
      });
    
  });
  
})




addEventListener("user-newInit", function()
{
  
  var page = pages['user-new.html']
  var  form = page.querySelector('form')
  
  console.log("form", form);
  
  form.addEventListener('submit', function(ev)
  {
    
    ev.preventDefault();
    ev.stopPropagation();
    
    let e = form.querySelector("[type=email]").value;
    let p1 = form.querySelector("[name=password]").value;
    let p2 = form.querySelector("[name=confirmation]").value;
    
    if ( p1 != p2 )
    {
      ons.notificaton.alert("Senhas devem coincidir");
      return
    }
    
    form.setAttribute("loading", true);
    
    auth.createUserWithEmailAndPassword(e, p1)
      .catch(function(error)
      {
        ons.notification.alert(error.message)
        form.removeAttribute("loading");
      });
    
    
  })
  
})

