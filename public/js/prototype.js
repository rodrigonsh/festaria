// festaria
// prototype by <rodrigo.nsh@gmail.com>

const MB2 = 2097152
const MB3 = 3145728
const MB10 = 10485760

var app = null
var pager = null
var navi = null

var uiConfig = null
var authUI = null

var auth = null
var db = null
var storage = null

var user = null
var isAdmin = false;

var day = {} // A club event with banners, list etc...
var currentDate = null // the timestamp of the current event

var $userAvatar = null
var page = null
var pages = []

var UID = null
var vendorUID = vendorUID

var $body = q('body')

// profile-list-item template
var $PLI = null

// Home elements
var $bannerVideoBtn = null
var $website = null
var $tickets = null
var $clubList = null
var $toggleUserInList = null

// You Guess it
var $map = null;

var videoURL = null

inCordova = false;
startTime = new Date().valueOf()

// this is where we keep a record for each profile lifted from firebase
var profiles = []

// personal information for the VENDOR
// currentUser personal can be found at personal[UID].whatever
var personal = {}

// fastLoad some data from storage into profiles
var club = storageJGet('club', 'null')
var uids = storageJGet('uids', '[]')

for(var i=0; i < uids.length; i++)
{
  let p = storageJGet(uids[i])
  if ( p != null ) profiles[ uids.length ] = p
}

function cacheUID(s)
{
  
  let info = s.val();
  
  if (info == null)
  {
    //console.error('cacheUID, info is null', s.key);
    return
  }
    
  
  //console.log('cacheUID', info.uid);
  
  let uid = info.uid
  
  if( uids.indexOf(uid) == -1 )
  {
    uids.push(uid);
  }
  
  profiles[uid] = info
  
  localStorage.setItem(uid, JSON.stringify(info));
  localStorage.setItem('uids', JSON.stringify(uids));
  
  databind();
  
}


function cacheUIDs( list )
{
 
  let keys = Object.keys( list );
   
  for( let i=0; i < keys.length; i++  )
  {
    
    var path = [ 'users', keys[i], vendorUID, 'info'].join("/");
    db.ref(path).once('value', cacheUID)
    
  }
  
}


function refreshProfilesInfos()
{
  
  let uidsToCache = {}
   
  for(var i=0; i < uids.length; i++) { uidsToCache[ uids[i] ] = true }
   
  if('list' in day)
  {
    keys = Object.keys(day.list);
    for(var i=0; i < keys.length; i++) { uidsToCache[ keys[i] ] = true }
  }
  
  if('reservations' in day)
  {
    keys = Object.keys(day.reservations);
    for(var i=0; i < keys.length; i++) { uidsToCache[ keys[i] ] = true }
  }
  
  cacheUIDs(uidsToCache);
  
}


function userProfilePic(p)
{
  
  let d = create("div")
  let img = create('img')
  
  d.className = "relative-container"
  d.appendChild(img);
  
  if ( p != null && 'photoURL' in p ) img.src = p.photoURL
  else if( p != null && 'posterURL' in p ) img.src = p.posterURL
  else img.src = "profile.png"
  
  img.classList.add('profile-pic')
  
  if ( undefined == p )
  {
      console.log('Soooooooooooooo');
  }
  else
  {
    
    if ( 'uid' in p )
    {
      img.dataset.bind = `profiles['${p.uid}'].photoURL`
      img.dataset.uid = p.uid
    }  
    
    
    if ( p != null && 'videoURL' in p )
    {
      d.classList.add('playsVideo');
      d.dataset.video = p.videoURL
      d.setAttribute('emit', 'watchVideo');
    }
    
  }
  
  
  
  
  img.onerror = function()
  {
    
    setTimeout(() => {
      
      let path = []
      path.push("users");
      path.push(this.dataset.uid);
      path.push(vendorUID);
      path.push("info");
      
      console.log("profile-pic error: fetching", path.join("/"));
      
      db.ref(path.join("/")).on("value", cacheUID)
      
      }, 20000);
    
    
    
  };
  
  return d
  
}

function loadingFailed()
{
  page.removeAttribute('loading');
  page.setAttribute('failed', true);
  setTimeout(function()
  {
    page.removeAttribute('error');
  }, 2000)
}

function loadingSuccess(message, toast)
{
  
  if (!message)
  {
    message = "OK" 
    toast = true
  }
  
  console.log("Success:" , message);
  
  if (toast)
  {
    ons.notification.toast(
    {
      'message': message ,
      'buttonLabel': 'OK',
      'timeout': 2000
    });
  }
    
  
  else
  {
    
    setTimeout(function()
    {
      page.removeAttribute('ok')
      emit("loadHome");
      
    }, 1000);
    
    ons.notification.alert(message);
  
  }
  
  
  page.removeAttribute('loading');
  page.setAttribute('ok', true);
  
}

function pageTo(id)
{
  
  
  // All this to avoid double page insertion
  
  //console.warn("pageTo", id);
  
  if (navi == null)
  {
    setTimeout(() => pageTo(id), 1000);
    return
  }
  
  var childNodes = navi.childNodes
  var pageAlreadyPushed = false;
  
  for(var i=0; i < childNodes.length; i++)
  {
    
    if( childNodes[i].id == id )
    {
      pageAlreadyPushed = true
    }
  }
  
  if ( pageAlreadyPushed )
  {
    
    console.warn("already loaded",id);
    
    navi
      .resetToPage(id).then( function(p)
      {
        
        setTimeout(function()
        {
          page = p
          var evName = page.id.replace('.html', '') + 'Init'
          emit(evName);
        }, 300);
        
        
      });
  }
  else
  {
    
    navi.pushPage(id)
    
  }
  
}





document.addEventListener('init', function(event) {
  
  page = event.target;
  pages[page.id] = page
  
  if (navi != null)
  {
    console.log('navi.querySelector(page.id)', navi.querySelector("#"+page.id))
  }
  
  
  emit(page.id.replace('.html', '')+'Init', page);
  databind();
  
  var menu = document.getElementById('sidemenu')
  menu.close()
  
});



/*addEventListener('prepush', (ev) => console.log('prepush', ev) )
addEventListener('postpush', (ev) => console.log('postpush', ev) )
addEventListener('prepop', (ev) => console.log('prepop', ev) )
*/
addEventListener('postpop', function(ev)
{
  var n = ev.target.topPage.id.replace(".html", "")+"Init";
  emit(n);
})


document.addEventListener('DOMContentLoaded', function()
{
  
  app = firebase.app();
  db = firebase.database();
  storage = firebase.storage();
  
  ons.ready(function()
  {
    
    //console.log('ons ready');
        
    $PLI = q('#profile-list-item')
    //console.log("PLI", $PLI);
        
    pager = q("#content");    
    
    if (!ons.platform.isIOS())
    {
       ons.platform.select("android")
    }
   
    var path = ['users', vendorUID, vendorUID, 'info'].join("/");
    db.ref(path).on("value", cacheUID)
    
    // Why here? Because we need to pageTo("auth") in case of no user
    auth = firebase.auth();
    auth.onAuthStateChanged(function(u)
    {
      user = u
      if (user)  emit('userReady');
      else emit('noUser');    
      
    })
      
  }); //ons.ready
    
});


addEventListener("confirm", function(ev)
{
  
  let evName = ev.originalEvent.data
  let msg = lang.confirmIrreversible+evName
  
  ons.notification.confirm(msg)
    .then(function(res){
      if(res) emit(evName);
    });

  
})



addEventListener("mainInit", function()
{
  navi = q("ons-navigator#navi");
})



addEventListener("toggleMenu", function()
{
  
  var menu = document.getElementById('sidemenu')
  menu.toggle()
  
})



addEventListener('startupInit', function()
{
 
  let now = new Date().valueOf();
  console.log("startupINIT", now, startTime, now - startTime );
  
  if ( ( now - startTime ) < 2000 )
  {
    
    
    let interval = 2000 - ( now - startTime )
    setTimeout( function()
    {
      q('#splash').setAttribute('hidden', true)
    }, interval);
    
    return
    
  }
  
  q('#splash').setAttribute('hidden', true)
  
  setTimeout(function(){
    
    document
      .getElementById('startup.html')
      .querySelector('[spinnar] span')
      .textContent = lang.soSlow
    
    
  }, 10000)
  
})



addEventListener('sidemenuInit', function()
{
  $userAvatar = q('#currentUserAvatar')
})



addEventListener('showControls', function(ev)
{
 
 ev.originalEvent.target.setAttribute('controls', true);
  
})



addEventListener('logOut', function()
{
  $body.removeAttribute('is-admin');
  auth.signOut()
})



addEventListener("about", function()
{
  pageTo("about.html");
})



addEventListener('editProfile', function()
{
  pageTo('profile-edit.html');
})



function saveProfile(ev)
{
  
  ev.preventDefault();
  ev.stopPropagation();
  
  page.setAttribute('loading', true);
  
  fd = new FormData(ev.target)
  
  //console.log( fd.get("displayName") );
  
  var video = q("#profileVideoInput").files[0];
  var image = q("#profileImageInput").files[0];
  
  var path = ['users', UID, vendorUID, 'info'].join("/");
  
  var newData = {
    uid: UID,
    bio: fd.get('bio'),
    displayName: fd.get('displayName')
  }

  //console.log(path, newData);
  
  if( UID in profiles )
  {
  
  db
    .ref( path )
    .update( newData )
    .then( function()
    { 
      loadingSuccess(lang.profileUpdated)
    })
    .catch( console.error );  
    
    
    var path = ['users', UID, vendorUID, 'personal'].join("/");
  
  db
    .ref(path)
    .update(
    {
      birthday: fd.get('birthday'),
      phone: fd.get('phone'),
      doc: fd.get('doc'),
      address: fd.get('address'),
      realName: fd.get('realName')
    })  
    
  }
  
  else
  {
    
    db
    .ref( path )
    .set( newData )
    .then( function()
    { 
      loadingSuccess(lang.profileUpdated)
    })
    .catch( console.error );  
    
    
    var path = ['users', UID, vendorUID, 'personal'].join("/");
  
  db
    .ref(path)
    .set(
    {
      birthday: fd.get('birthday'),
      phone: fd.get('phone'),
      doc: fd.get('doc'),
      address: fd.get('address'),
      realName: fd.get('realName')
    })  
    
  }
  
  
    
    
    
 
  if ( video )
  {
    //console.log("a video will be uploaded");
  
    if ( video.size > MB10 )
    {
      ons.notification.alert(lang.max10MB);
      loadingFailed();   
      return
    } 
  
    var path = "videos/"+UID+"/"+vendorUID
    
    storage.ref(path)
    .put(video)
    .then( function()
    {
      
      loadingSuccess(lang.videoSent, true);
           
      storage
        .ref(path)
        .getDownloadURL()
        .then( function(url)
        {
          
          //console.log('got Download URL', url)
          
          var path = [
            'users', 
            UID, 
            vendorUID, 
            'info',
            'videoURL'
            ].join("/");
          
          //console.log(path, url);
          
          db
            .ref(path)
            .set(url)
            .then(function()
            {
              
              emit("clubChanged");
              loadingSuccess();
              
            })
            .catch(console.error);
          
        })
        .catch(console.error)
      
    })
    .catch(console.error)
    
     
  
  }

  if (image)
  {
    
    if ( image.size > MB3 )
    {
      ons.notification.alert(lang.max3MB);
      loadingFailed();   
      return
    } 
    
    var path = "fotos/"+UID+"/"+vendorUID
    
    storage.ref(path)
    .put(image)
    .then( function()
    {
      
      loadingSuccess(lang.photoSent, true);
           
      storage
        .ref(path)
        .getDownloadURL()
        .then( function(url)
        {
          
          console.log('got Download URL', url)
          
          q('#currentUserAvatar').src = url
          
          user.photoURL = url
          
          var path = [
            'users', 
            UID, 
            vendorUID, 
            'info',
            'photoURL'
            ].join("/");
          
          console.log(path, url);
          
          db
            .ref(path)
            .set(url)
            .catch(console.error);
          
        })
        .catch(console.error)
      
    })
    .catch(console.error)
    
  }
  
}



addEventListener('profile-editInit', function()
{
  page = pages['profile-edit.html']
  var profileForm = page.querySelector('form')
  profileForm.addEventListener( 'submit', saveProfile )
  
})



addEventListener("uidsLoaded", function()
{
  
  for( var i=0; i < uids.length; i++ )
  {
    profiles[uids[i]] = storageJGet(uids[i]);
  }
  
  databind();
  
})





function makeProfileListItem(p, k)
{
  
  let node = document.importNode($PLI.content, true);
  let li = node.querySelector("ons-list-item");
  let l = node.querySelector(".left");
  let c = node.querySelector(".center");
  let name = node.querySelector("span");
  let bio = node.querySelector("festaria-bio");
  let profilePic = false;
  
  profilePic = userProfilePic(p);
  
  l.appendChild(profilePic);
  
  if (p == null)
  {
    
    name.dataset.bind = `profiles['${k}'].displayName`
    bio.dataset.bind = `profiles['${k}'].bio`
     
  } else {
    
    if ( 'uid' in p )
    {
      li.dataset.uid = p.uid
    
      name.dataset.bind = `profiles['${p.uid}'].displayName`
      bio.dataset.bind = `profiles['${p.uid}'].bio`
    }
    
  }
  
  return li
}


function renderProfiles(keys, $dest)
{
  if ( $dest == null )
  {
    return setTimeout( function(){ emit('renderProfiles') }, 1000 )
  }
  
  $dest.innerHTML = ""
    
  for( var i = 0 ; i < keys.length; i++ )
  {
    
    console.log(i, 'rendering', keys[i]);
    
    let p = profiles[keys[i]]
    
    
    if ($dest.id == "clubList")
    {
      li = makeProfileListItem(p, keys[i], $dest);
      $dest.appendChild(li)  
    } else {
      card = makeProfileCard(p);
      $dest.appendChild(card)
    }
    
    
  }
  
}


addEventListener('loadHome', function()
{
  
  if ( club == null ) return;
  
  var days = Object.keys(club.dates)
  currentDate = parseInt(days[0])
  
  if ( page != null )
  {
    pageTo("home.html");
  }
  else
  {
    renderClub();
  }
})


function renderClub()
{
  
  if ( UID == null || page == null )
  {
    setTimeout(renderClub, 2000);
    return
  }
    
  if ( undefined ==  profiles[vendorUID] )
  {
    console.error("renderClub: undefined == profiles[vendorUID]", vendorUID);
    setTimeout(renderClub, 1000);
    return
  }
  
  if ( $bannerVideoBtn == null )
  {
    console.error("home is not ready");
    setTimeout(renderClub, 1000);
    return;
  }
  
  $clubList.innerHTML = ""
  
  $tickets = q("#tickets");
  $website = q("#website");
  
  let days = Object.keys(club.dates)
  day = club.dates[days[0]]
  
  $banner.src = day.posterURL
  
  if ( day.tickets ) $tickets.removeAttribute('hidden');
  else $tickets.setAttribute('hidden', true);
  
  if ( 'website' in day ) $website.removeAttribute('hidden');
  else $website.setAttribute('hidden', true);
  
  if ( 'videoURL' in day )
  {
    $bannerVideoBtn.removeAttribute('hidden'); 
    $bannerVideoBtn.dataset.video = day.videoURL
  }
  else $bannerVideoBtn.setAttribute('hidden', true);
  
  
  if ('list' in day)
  {
    
    page.removeAttribute("emptyList");
    $toggleUserInList.textContent = lang.listAdd
  
    listUIDs = Object.keys(day.list)
    
    // TODO: Sort by displayName
    for(var i=0; i < listUIDs.length; i++)
    {
      
      // If the this uid in the list is "true" ...
      if( day.list[listUIDs[i]]  )
      {
        
        let prof = profiles[listUIDs[i]]
        let li = makeProfileListItem(prof, listUIDs[i])
        
        if ( listUIDs[i] == UID )
        {
          $toggleUserInList.textContent = lang.listRemove
        } 
        
        $clubList.appendChild(li);
        
      }
      
    }
    
  }
  else
  {
    page.setAttribute("emptyList", true);
  }
  
  
  page.removeAttribute('loading')
  
  if ( 'birthday' in profiles[UID]  )
  {
    let now = new Date()
    let m = now.getMonth() +1 
    let d = now.getDate()
    
    m = ( m < 10 ) ? "0"+m : m
    d = ( d < 10 ) ? "0"+d : d
    
    let today = [m, d].join("-");
    let birthday = profiles[UID].birthday.substr(5);
    
    if ( today ==  birthday)
    {
      page.setAttribute('cakeday', true);
    }
  }
  
  if ( document.getElementById("startup.html" ))
  {
    document.getElementById("startup.html").removeAttribute('loading');
  }
  
  databind();
  
}



addEventListener("homeInit", function(){
  
  page = pages['home.html']
  
  $banner = page.querySelector("#banner");
  $bannerVideoBtn = page.querySelector("#bannerVideoBtn");
  $tickets = page.querySelector("#tickets")
  $website = page.querySelector("#website")
  $clubList = page.querySelector("#clubList")
  $toggleUserInList = page.querySelector("#toggleUserInList")
  
  
  renderClub();
  
})



addEventListener("buyTickets", function()
{
  let days = Object.keys(club.dates)
  let day = club.dates[days[0]]
  window.open(day.tickets)
})



addEventListener("visitWebsite", function()
{
  let days = Object.keys(club.dates)
  let day = club.dates[days[0]]
  window.open(day.website)
})



addEventListener("toggleUserInList", function(ev)
{
  let days = Object.keys(club.dates)
  let day = club.dates[days[0]]
  
  let dtFmt = moment(parseInt(days[0])).format("YYYY-MM-DD ");
  dtFmt = dtFmt + club.hora_limite + ":00"
  
  let maxDate = moment(dtFmt)
  
  //console.log('maxDate', maxDate.format(), dtFmt);
  //console.log('current', moment().format());
  
  if ( new moment().valueOf() > maxDate.valueOf() )
  {
    let maxDateFmt = maxDate.format("LLL")
    let msg = lang.listTimeout+maxDateFmt
    ons.notification.alert(msg);
    return;
  }
  
  if ( "list" in day )
  {
    if ( UID in day.list ) nextVal = ! day.list[ UID ]
    else nextVal = true

    
  } else { nextVal = true  }
  
  
  
  var path = []
  path.push("clubs");
  path.push(vendorUID);
  path.push("dates");
  path.push(days[0]);
  path.push("list");
  path.push(UID);
  
  //console.log(path.join("/"))
  
  db.ref( path.join("/") )
    .set( nextVal )
    .then( function(res){
      renderClub();
      setTimeout(databind, 1000);
       } )
    .catch( console.error );
  
})



addEventListener("clubChanged", function()
{
  
  if( club == null ) return;
    
  q('#splash').setAttribute('hidden', true);
    
  let days = Object.keys(club.dates)
  
  if (currentDate == null) currentDate = parseInt(days[0])
  
  day = club.dates[currentDate] 
  day.fulldate = moment(currentDate).format("LLL");
  
  //console.log("day", day);
  
  refreshProfilesInfos();
  
  setTimeout(function()
  {
    let pageName = page.id.replace(".html", "");
    emit(pageName+'Init')
    databind();
  }, 300);
    

  
})



addEventListener("aboutInit", function()
{
  page = pages['about.html']
  
  $map = page.querySelector("#map");
  
  var map = L.map('map').setView(club.coords, 16);

  L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png', {
      attribution: '&copy; Map tiles by Carto, under CC BY 3.0. Data by <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>, under ODbL'
  }).addTo(map);

  L.marker(club.coords).addTo(map)
      .bindPopup(club.name)
      .openPopup();
   
})




addEventListener("editDates", function()
{
  pageTo("dates-list.html");
})



addEventListener("dates-listInit", function()
{
  
  page = pages['dates-list.html']
  
  let $list = page.querySelector("#datesList");
  $list.innerHTML = ""
  
  let days = Object.keys(club.dates);
  
  for( let i=0; i < days.length; i++ )
  {
    
    let li = create('ons-list-item');
    let l = create('div');
    let c = create('div');
    
    l.className = "left"
    c.className = "center"
    
    li.setAttribute('emit', 'editDate')
    li.setAttribute('tappable', true)
    li.setAttribute('modifier', 'chevron')
    li.dataset.ts = parseInt(days[i])
    
    let data = moment(parseInt(days[i]))
    
    l.textContent = data.format("DD-ddd");
    
    c.textContent = club.dates[days[i]].name
    
    li.appendChild(l);
    li.appendChild(c);
    
    $list.appendChild(li);
    
    
  }
  
})




addEventListener('editDate', function(ev)
{
  let target = getTarget(ev.originalEvent.target, 'ts')
  currentDate = parseInt(target.dataset.ts)
  console.log('lets edit', currentDate);
  pageTo('date-edit.html');
})


addEventListener("newDate", function(ev)
{
  currentDate = moment().valueOf()
  pageTo("date-edit.html");
})


function saveDate(ev)
{
  
  ev.preventDefault()
  ev.stopPropagation()
  
  var video = q("#eventVideoInput").files[0];
  var image = q("#eventImageInput").files[0];
  var audio = q("#eventAudioInput").files[0];
  
  page.setAttribute("loading", true);
  
  let form = new FormData( page.querySelector('form') )
  
  if ( ! ('dates' in club ) ) club.dates = {}
  
  day = club.dates[currentDate]
  
  if ( undefined == day )
  {
    let dt = form.get("date")
    let tm = form.get("time")
    
    currentDate = moment(dt+" "+tm+":00").valueOf();
    
    day = {"name": "new", "list": {}, "release": "😄" }
    
  }

  day.name = form.get('name')
  day.release = form.get('release')
  day.tickets = form.get('tickets')
  day.website = form.get('website')
    
  let path = ['clubs', vendorUID, "dates", currentDate]
  
  console.log(path.join('/'), day);
  
  db
    .ref( path.join("/") )
    .set( day )
    .then( loadingSuccess )
    .catch( loadingFailed )
  
  
  if ( video )
  {
    console.log("a video will be uploaded");
  
    if ( video.size > MB10 )
    {
      ons.notification.alert(lang.max10MB);
      loadingFailed();   
      return
    }
    
    var adsRef = "ads/"+UID+"/"+currentDate 
  
    storage.ref(adsRef)
    .put(video)
    .then( function()
    {
      
      loadingSuccess(lang.videoSent, true);
           
      storage
        .ref(adsRef)
        .getDownloadURL()
        .then( function(url)
        {
          
          let path = ['clubs', vendorUID, 'dates', currentDate, 'videoURL']         
          
          db
            .ref(path.join("/"))
            .set(url)
            .then(loadingSuccess)
            .catch(console.error);
          
        })
        .catch(console.error)
      
    })
    .catch(console.error)
      
     
  
  }


  if ( image )
  {
    
    if ( image.size > MB2 )
    {
      ons.notification.alert(lang.max2MB);
      loadingFailed();   
      return
    } 
    
    let path = ['posters', vendorUID, currentDate]     
    let posterRef = path.join("/");
    
    storage.ref(posterRef)
    .put(image)
    .then( function()
    {
      
      loadingSuccess(lang.posterSent, true);
           
      storage
        .ref(posterRef)
        .getDownloadURL()
        .then( function(url)
        {
          
          let path = ['clubs', vendorUID, 'dates', currentDate, 'posterURL']
          
          db
            .ref(path.join("/"))
            .set(url)
            .then(loadingSuccess)
            .catch(console.error);
          
        })
        .catch(console.error)
      
    })
    .catch(console.error)
    
  }
  
  
  if ( audio )
  {
    
    if ( audio.size > MB2 )
    {
      ons.notification.alert(lang.max2MB);
      loadingFailed();   
      return
    } 
    
    let path = ['audios', vendorUID, currentDate]     
    let audioRef = path.join("/");
    
    storage.ref(audioRef)
    .put(audio)
    .then( function()
    {
      
      loadingSuccess(lang.audioSent, true);
           
      storage
        .ref(audioRef)
        .getDownloadURL()
        .then( function(url)
        {
          
          let path = ['clubs', vendorUID, 'dates', currentDate, 'audioURL']
          
          db
            .ref(path.join("/"))
            .set(url)
            .then(loadingSuccess)
            .catch(console.error);
          
        })
        .catch(console.error)
      
    })
    .catch(console.error)
    
  }
  
}


addEventListener("date-editInit", function()
{
  
  page = pages['date-edit.html']
  
  var form = page.querySelector("form");
  form.className = '';
  form.addEventListener("submit", saveDate);
  
  // fill form with data
  var dt = moment(currentDate).format("YYYY-MM-DD"); 
  var tm = moment(currentDate).format("HH:mm"); 

  if ( ! ( 'dates' in club ) )
  {
    club.dates = {}
  }

  if ( currentDate in  club.dates ) 
  {
    day = club.dates[currentDate]    
  }
  else
  {
    day = {}
    form.className ='new';
    form.querySelector("[type=date]").value = dt;
    form.querySelector("[type=time]").value = tm;
  } 
  
  
  
})



addEventListener("deleteDate", function()
{
  
  var path = ['clubs', vendorUID, "dates", currentDate]
  
  db
    .ref( path.join("/") )
    .set( null )
    .then( loadingSuccess )
    .catch( loadingFailed )
    
  path = ['ads', vendorUID, currentDate].join("/");
  storage.ref( path ).delete()
    
  path = ['posters', vendorUID, currentDate].join("/");
  storage.ref( path ).delete()
    
  path = ['audios', vendorUID, currentDate].join("/");
  storage.ref( path ).delete()
    
})






addEventListener("watchVideo", function(ev)
{
  
  var t = getTarget(ev.originalEvent.target, 'video')
  videoURL = t.dataset.video
  
  console.log(t);
  
  pageTo("video.html");
  
})



addEventListener("support", () => pageTo("support.html") )





addEventListener("clubConfig", function()
{
  if ( UID != vendorUID ) return;
  pageTo("club-config.html");
})


addEventListener("club-configInit", function()
{
  
  var frm = pages['club-config.html'].querySelector('form');
  
  frm.addEventListener('submit', function(ev)
  {
    
    if ( UID != vendorUID ) return;
    
    ev.preventDefault();
    ev.stopPropagation();
    
    frm.setAttribute('loading', true);
    
    let fdata = new FormData(frm);
    
    let update = 
    {
      'name' : fdata.get('name'),
      'address' : fdata.get('address'),
      'telefone' : fdata.get('phone'),
      'cakeday' : fdata.get('cakeday'),
      'hora_limite' : fdata.get('time'),
      'coords':
      {
        'lat': parseFloat(fdata.get('latitude')),
        'lng': parseFloat(fdata.get('longitude'))
      }
    }
    
    db
      .ref( 'clubs/'+vendorUID )
      .update( update )
      .then(function()
      {
        
        loadingSuccess(lang.clubUpdated);
        
      });
    
    
    
    var image = q("#vipMapInput").files[0];
    
    if ( image )
    {
      
      if ( image.size > MB2 )
      {
        ons.notification.alert(lang.max2MB);
        loadingFailed();   
        return
      } 
      
      storage.ref('vipMaps/'+vendorUID)
      .put(image)
      .then( function()
      {
        
        loadingSuccess(lang.vipMapSent, true);
             
        storage
          .ref('vipMaps/'+vendorUID)
          .getDownloadURL()
          .then( function(url)
          {
            
            let path = ['clubs', vendorUID, 'vipMapURL']
            
            db
              .ref(path.join("/"))
              .set(url)
              .then(loadingSuccess)
              .catch(console.error);
            
          })
          .catch(console.error)
        
      })
      .catch(console.error)
      
    }
    
    
  })
  
  
  
})





addEventListener("switchLang", function()
{
  
  let langKeys = Object.keys(langs);
  
  let langIndex = langKeys.indexOf( currentLanguage )
  langIndex++
  
  if ( langIndex >= langKeys.length )
  {
    langIndex = 0
  }
  
  currentLanguage = langKeys[langIndex]
  lang = langs[currentLanguage]
  databind();
  
  moment.locale(currentLanguage)
  localStorage.setItem('lang', currentLanguage);
  
})

document.addEventListener("deviceready", function()
{
  
  console.log("device is ready")
  inCordova = true
  
})
