const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

exports.setCustomClaims = functions.https.onCall((data, context) => {
  //console.log(context.auth.uid, data.vendorUID);
  return new Promise((resolve, reject) =>
  {
    admin.auth().setCustomUserClaims(context.auth.uid, data)
    .then(() => resolve("por aqui tudo bem graças a Deus"))
    .catch((err) => reject(err))
  })

});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// TODO: portar esta funcao para dados privados por vendor
/*exports.novoUsuario = functions.auth.user().onCreate((user) => {
  
  if( ! user.displayName )
  {
    user.displayName = "Membro Novo"
  }
  
  if( ! user.photoURL )
  {
    user.photoURL = "profile.png"
  }
  
  
  
  return admin.database().ref("users/"+user.uid+"/info").set({
    uid: user.uid,
    displayName: user.displayName,
    photoURL: user.photoURL,
    bio: "Ainda não escrevi minha mensagem"
  });
  
});*/




