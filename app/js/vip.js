var vip = null
var vipID = null


addEventListener("loadReservations", function()
{
  pageTo("reservations.html")
})



addEventListener("adminReservations", function()
{
  if (UID !== vendorUID) return
  pageTo("reservations-admin.html")
})



addEventListener("reservationsInit", function()
{
  
  if ( ! ("vips" in club) ) return
  
  page = pages['reservations.html']
  
  $list = page.querySelector("ons-list");
  $list.innerHTML = ""
  
  var clients = {}
  
  if ('reservations' in day)
  {
    clients = Object.keys(day.reservations)
  }  
    
  var vips = Object.keys(club.vips)
  for( var i=0; i < vips.length; i++ )
  {
    
    //console.log("lets make", vips[i], club.vips[vips[i]]);
    
    let li = makeProfileListItem( club.vips[vips[i]] , null );
    
    li.classList.add("available");
    li.dataset.vip = vips[i]
    li.setAttribute('emit', 'reserveVIP')
    
    let span = li.querySelector("span");
    let bio = li.querySelector("festaria-bio");
    
    span.dataset.bind = `club.vips['${vips[i]}'].name`
    bio.dataset.bind = `club.vips['${vips[i]}'].price`
    
    for( var j=0; j < clients.length; j++ )
    {
      
      let reservation = day.reservations[clients[j]]
      
      // Recreate list item with profile info
      if ( reservation.vip == vips[i] )
      {
        
        li = makeProfileListItem( profiles[clients[j]], clients[j] )
        
        span = li.querySelector("span");
        
        if ( reservation.status == 1 )
        {
          li.classList.add('confirmed');
          span.textContent = lang.confirmed
        }
        
        else
        {
          li.classList.add("reserved");
          span.textContent = lang.reserved
        }
        
        
        bio = li.querySelector("festaria-bio");
        bio.dataset.bind = `club.vips['${vips[i]}'].name`
        
        
        delete span.dataset.bind
        
        if( clients[j] == UID )
        {
          li.setAttribute('emit', 'freeVIP')
          
          li.querySelector('.relative-container')
            .classList
            .remove('playsVideo');
            
          li.querySelector('.relative-container')
            .removeAttribute("emit");
            
        }
        
        break;
        
      }
    }
    
    $list.appendChild(li);
    
  }
  
  databind();
  
  
})



addEventListener("reservations-adminInit", function()
{
  
  // load personal info here
  if ( ! ("vips" in club) ) return
  
  page = pages['reservations-admin.html']
  
  $list = page.querySelector("ons-list");
  $list.innerHTML = ""
  
  var clients = {}
  
  if ('reservations' in day)
  {
    clients = Object.keys(day.reservations)
  }  
    
  var vips = Object.keys(club.vips)
  
    
  for( var j=0; j < clients.length; j++ )
  {
    
    // load personal
    let path = ['users', clients[j], vendorUID, 'personal'].join('/');
    db.ref(path).on('value', function(s)
    {
      
      //console.log(s.ref.parent.path.pieces_[1]);
      
      let uid = s.ref.parent.path.pieces_[1]
      
      personal[uid] = s.val();
      databind();
      
    });
    
    li = makeProfileListItem( profiles[clients[j]], clients[j] )
    
    li.setAttribute('emit', 'reservationDetail');
    li.setAttribute('tappable', true)
    li.setAttribute("modifier", "chevron")
    
    let vip = day.reservations[clients[j]].vip
    let meta = club.vips[vip]
    
    bio = li.querySelector("festaria-bio");
    bio.dataset.bind = `club.vips['${vip}'].name`
    
    if ( day.reservations[clients[j]].status == 1 )
    {
      li.classList.add('confirmed');
    }
    
    $list.appendChild(li);
    
  }
  
  databind();
  
})



addEventListener("reserveVIP", function(ev)
{
  
  let target = getTarget( ev.originalEvent.target , "vip" )
  
  // cant change if there is a vip confirmed for you
  if ( 
    'reservations' in day && 
    UID in day.reservations && 
    day.reservations[UID].status == 1 
    ) return
  
  let path = ["clubs", vendorUID, "dates", currentDate, "reservations", UID]
  path = path.join("/");
  
  db.ref(path)
    .set({
      'status': 0,
      'vip': target.dataset.vip,
      'createdAt': moment().valueOf()
    })
    .then(function()
    {
      setTimeout( () => emit("reservationsInit"), 100 );
      loadingSuccess(lang.wonderful, true) 
    });
  
})



addEventListener("freeVIP", function(ev)
{
  // TODO: if the user frees the vip and the payment is confirmed
  // then the vip becomes "free" like a gift
  // the user bought the vip and gives it for free so that it may be
  // freely used
  
  // cant change if there is a vip confirmed for you
  if ( 
    'reservations' in day && 
    day.reservations[UID].status == 1 
    ) return
  
  let path = ["clubs", vendorUID, "dates", currentDate, "reservations", UID]
  path = path.join("/");
  
  db.ref(path)
    .set(null)
    .then(function(){ loadingSuccess(lang.noWorries, true) });
  
})



addEventListener("reservationDetail", function(ev)
{
  let target = getTarget(ev.originalEvent.target, 'uid')
  day.detail = target.dataset.uid
  pageTo("reservation-detail.html");
})



addEventListener("reservation-detailInit", function()
{
  
  databind();
  
})



addEventListener('removeReserve', function(ev)
{
  
  var path = [
  'clubs', 
  vendorUID, 
  'dates', 
  currentDate, 
  'reservations', 
  day.detail].join("/")
  
  db.ref(path).set(null).then(function()
  {
    pageTo("reservations-admin.html");
  });
  
})



addEventListener('confirmReserve', function(ev)
{
  
  var path = [
  'clubs', 
  vendorUID, 
  'dates', 
  currentDate, 
  'reservations', 
  day.detail,
  'status'].join("/")
  
  db.ref(path).set(1).then(function()
  {
    pageTo("reservations-admin.html");
  });
  
})


addEventListener("vipAdmin", () => pageTo("vip-admin.html") )

addEventListener("vip-adminInit", function()
{
  
  if ( UID != vendorUID ) return;
  
  if ( ! ("vips" in club) ) return
  
  page = pages['vip-admin.html']
  
  $list = page.querySelector("ons-list");
  $list.innerHTML = ""
  
  var vips = Object.keys(club.vips)
  for( var i=0; i < vips.length; i++ )
  {
    
    let li = create( "ons-list-item" );
    li.dataset.vip = vips[i]
    li.setAttribute("tappable", true);    
    li.setAttribute('emit', 'vipEdit')
    li.setAttribute('modifier', 'chevron')
    
    li.textContent = club.vips[vips[i]].name
    
    $list.appendChild(li);
    
  }
  
  databind();
})


addEventListener("vipEdit", function(ev)
{
  
  if ( UID != vendorUID ) return;
  
  let target = getTarget(ev.originalEvent.target, 'vip')
  vipID = target.dataset.vip
  vip = club.vips[vipID]
  console.log("edit VIP", vipID)
  pageTo("vip-edit.html")
  
})

addEventListener("vip-editInit", function()
{
  
  if ( UID != vendorUID ) return
  
  page = pages['vip-edit.html']
  
  var form = page.querySelector("form")
  form.addEventListener("submit", function(ev)
  {
    
    console.log("form on submit");
    
    ev.preventDefault()
    ev.stopPropagation()
    
    var fd = new FormData(form);
    
    vip.name = fd.get("name")
    vip.price = fd.get("price")
    
    let path = ['clubs', vendorUID, 'vips', vipID].join("/");
    
    db.ref(path).set(vip).then(function()
    {
      loadingSuccess(lang.clubUpdated);
    });
    
    
  })
  
  
})

addEventListener("vipAdd", function()
{
  if ( UID != vendorUID ) return
  vipID = moment().valueOf()
  vip = { 'name': '', 'price': '' }
  pageTo('vip-edit.html');
})

addEventListener("vipDelete", function()
{
  if ( UID != vendorUID ) return
  let path = ['clubs', vendorUID, 'vips', vipID].join("/");
  db.ref(path).set(null).then(function()
  {
    loadingSuccess(lang.clubUpdated);
  });
})
