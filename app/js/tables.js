var table = null
var tableID = null


addEventListener("loadTables", function()
{
  pageTo("tables.html")
})



addEventListener("manageTables", function()
{
  if (UID !== vendorUID) return
  pageTo("tables-manage.html")
})



addEventListener("tablesInit", function()
{
  
  if ( ! ("tables" in club) ) return
  
  page = pages['tables.html']
  
  $list = page.querySelector("ons-list");
  $list.innerHTML = ""
  
  var clients = {}
  
  if ('tables' in day)
  {
    clients = Object.keys(day.tables)
  }  
    
  var tables = Object.keys(club.tables)
  for( var i=0; i < tables.length; i++ )
  {
    
    //console.log("lets make", tables[i], club.tables[tables[i]]);
    
    let li = makeProfileListItem( club.tables[tables[i]] , null );
    
    li.classList.add("available");
    li.dataset.table = tables[i]
    li.setAttribute('emit', 'reserveTable')
    
    let span = li.querySelector("span");
    let bio = li.querySelector("festaria-bio");
    
    span.dataset.bind = `club.tables['${tables[i]}'].name`
    bio.dataset.bind = `club.tables['${tables[i]}'].price`
    
    for( var j=0; j < clients.length; j++ )
    {
      
      let reservation = day.tables[clients[j]]
      
      // Recreate list item with profile info
      if ( reservation.table == tables[i] )
      {
        
        li = makeProfileListItem( profiles[clients[j]], clients[j] )
        
        span = li.querySelector("span");
        
        if ( reservation.status == 1 )
        {
          li.classList.add('confirmed');
          span.textContent = lang.confirmed
        }
        
        else
        {
          li.classList.add("reserved");
          span.textContent = lang.reserved
        }
        
        
        bio = li.querySelector("festaria-bio");
        bio.dataset.bind = `club.tables['${tables[i]}'].name`
        
        
        delete span.dataset.bind
        
        if( clients[j] == UID )
        {
          li.setAttribute('emit', 'freeTable')
          
          li.querySelector('.relative-container')
            .classList
            .remove('playsVideo');
            
          li.querySelector('.relative-container')
            .removeAttribute("emit");
            
        }
        
        break;
        
      }
    }
    
    $list.appendChild(li);
    
  }
  
  databind();
  
  
})



addEventListener("tables-manageInit", function()
{
  
  // load personal info here
  if ( ! ("tables" in club) ) return
  
  page = pages['tables-manage.html']
  
  $list = page.querySelector("ons-list");
  $list.innerHTML = ""
  
  var clients = {}
  
  if ('tables' in day)
  {
    clients = Object.keys(day.tables)
  }  
    
  var tables = Object.keys(club.tables)
  
    
  for( var j=0; j < clients.length; j++ )
  {
    
    // load personal
    let path = ['users', clients[j], vendorUID, 'personal'].join('/');
    db.ref(path).on('value', function(s)
    {
      
      //console.log(s.ref.parent.path.pieces_[1]);
      
      let uid = s.ref.parent.path.pieces_[1]
      
      personal[uid] = s.val();
      databind();
      
    });
    
    li = makeProfileListItem( profiles[clients[j]], clients[j] )
    
    li.setAttribute('emit', 'tableDetail');
    li.setAttribute('tappable', true)
    li.setAttribute("modifier", "chevron")
    
    let table = day.tables[clients[j]].table
    
    bio = li.querySelector("festaria-bio");
    bio.dataset.bind = `club.tables['${table}'].name`
    
    if ( day.tables[clients[j]].status == 1 )
    {
      li.classList.add('confirmed');
    }
    
    $list.appendChild(li);
    
  }
  
  databind();
  
})



addEventListener("reserveTable", function(ev)
{
  
  let target = getTarget( ev.originalEvent.target , "table" )
  
  // cant change if there is a table confirmed for you
  if ( 
    'tables' in day && 
    UID in day.tables && 
    day.tables[UID].status == 1 
    ) return
  
  let path = ["clubs", vendorUID, "dates", currentDate, "tables", UID]
  path = path.join("/");
  
  db.ref(path)
    .set({
      'status': 0,
      'table': target.dataset.table,
      'createdAt': moment().valueOf()
    })
    .then(function()
    {
      setTimeout( () => emit("tablesInit"), 100 );
      loadingSuccess(lang.wonderful, true) 
    });
  
})



addEventListener("freeTable", function(ev)
{
  // TODO: if the user frees the table and the payment is confirmed
  // then the table becomes "free" like a gift
  // the user bought the table and gives it for free so that it may be
  // freely used
  
  // cant change if there is a table confirmed for you
  if ( 
    'tables' in day && 
    day.tables[UID].status == 1 
    ) return
  
  let path = ["clubs", vendorUID, "dates", currentDate, "tables", UID]
  path = path.join("/");
  
  db.ref(path)
    .set(null)
    .then(function(){ loadingSuccess(lang.noWorries, true) });
  
})



addEventListener("tableDetail", function(ev)
{
  let target = getTarget(ev.originalEvent.target, 'uid')
  day.detail = target.dataset.uid
  pageTo("table-detail.html");
})



addEventListener("table-detailInit", function()
{
  
  databind();
  
})



addEventListener('removeTableReservation', function(ev)
{
  
  var path = [
  'clubs', 
  vendorUID, 
  'dates', 
  currentDate, 
  'tables', 
  day.detail].join("/")
  
  db.ref(path).set(null).then(function()
  {
    pageTo("tables-manage.html");
  });
  
})



addEventListener('confirmTable', function(ev)
{
  
  var path = [
  'clubs', 
  vendorUID, 
  'dates', 
  currentDate, 
  'tables', 
  day.detail,
  'status'].join("/")
  
  db.ref(path).set(1).then(function()
  {
    pageTo("tables-manage.html");
  });
  
})


addEventListener("tablesAdmin", () => pageTo("tables-admin.html") )

addEventListener("tables-adminInit", function()
{
  
  if ( UID != vendorUID ) return;
  
  if ( ! ("tables" in club) ) return
  
  page = pages['tables-admin.html']
  
  $list = page.querySelector("ons-list");
  $list.innerHTML = ""
  
  var tables = Object.keys(club.tables)
  for( var i=0; i < tables.length; i++ )
  {
    
    let li = create( "ons-list-item" );
    li.dataset.table = tables[i]
    li.setAttribute("tappable", true);    
    li.setAttribute('emit', 'tableEdit')
    li.setAttribute('modifier', 'chevron')
    
    li.textContent = club.tables[tables[i]].name
    
    $list.appendChild(li);
    
  }
  
  databind();
})


addEventListener("tableEdit", function(ev)
{
  
  if ( UID != vendorUID ) return;
  
  let target = getTarget(ev.originalEvent.target, 'table')
  tableID = target.dataset.table
  table = club.tables[tableID]
  console.log("edit table", tableID)
  pageTo("table-edit.html")
  
})

addEventListener("table-editInit", function()
{
  
  if ( UID != vendorUID ) return
  
  page = pages['table-edit.html']
  
  var form = page.querySelector("form")
  form.addEventListener("submit", function(ev)
  {
    
    console.log("form on submit");
    
    ev.preventDefault()
    ev.stopPropagation()
    
    var fd = new FormData(form);
    
    table.name = fd.get("name")
    table.price = fd.get("price")
    
    let path = ['clubs', vendorUID, 'tables', tableID].join("/");
    
    db.ref(path).set(table).then(function()
    {
      loadingSuccess(lang.clubUpdated, true);
      emit("tablesAdmin");
    });
    
    
  })
  
  
})

addEventListener("tableAdd", function()
{
  if ( UID != vendorUID ) return
  tableID = moment().valueOf()
  table = { 'name': '', 'price': '' }
  pageTo('table-edit.html');
})

addEventListener("tableDelete", function()
{
  if ( UID != vendorUID ) return
  let path = ['clubs', vendorUID, 'tables', tableID].join("/");
  db.ref(path).set(null).then(function()
  {
    loadingSuccess(lang.clubUpdated);
  });
})
