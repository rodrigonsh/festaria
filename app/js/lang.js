langs = 
{
  
  'pt-br':
  {
    
    'connecting': 'Conectando...',
    'start': 'Começar',
    'loading': 'Carregando...',
    'soSlow': 'Paciência é Virtude',
    'exclusiveAccess': 'Acesso Exclusivo',
    'profileNetwork': 'Perfil Festaria',
    'loginNetwork': 'Login na Rede Festaria',
    'email': 'email',
    'password': 'senha',
    'passwordConfirmation': 'confirme a senha',
    'newAccount': 'Nova Conta',
    'logIn': 'Entrar',
    'logOut': 'Sair',
    'disclaimer': 'Ao prosseguir você concorda com os',
    'tos': 'Termos de Uso',
    'pp': 'Política de Privacidade',
    'needHelp': 'Precisa de Ajuda?',
    'newUserNetwork': 'Novo usuário na rede Festaria',
    'cancel': 'Cancelar',
    'register': 'Cadastrar',
    
    'publicInfo': 'Informações Públicas',
    'privateInfo': 'Informações Pessoais',
    
    'publicInfoDisclaimer': 'Acessíveis a todos os usuários deste app',
    'privateInfoDisclaimer': 'Dados compartilhados entre você e o estabelecimento',
    
    'name': 'Nome',
    
    'displayName': 'Nome / Apelido',
    'displayNamePH': "Aqui você pode zuar",
    
    'bio': 'Mensagem de Apresentação',
    'bioDefault ': 'Ainda não escrevi minha mensagem 😄',
    
    'updatePhotoURL': 'Atualizar Foto',
    'updateVideoURL': 'Atualizar Vídeo',
    
    'max2MB': 'Tamanho Máximo: 2MB',
    'max3MB': 'Tamanho Máximo: 3MB',
    'max10MB': 'Tamanho Máximo: 10MB',
    
    
    'realName': 'Nome Real',
    'realNamePH': 'Aqui não pode zuar',
    
    'doc': 'Nº Documento',
    'docPH': 'Nem aqui',
    
    'birthDate': 'Data de Nascimento',
    
    'phone': 'Número de Telefone',
    'phonePH': 'Também é importante',
    
    'address': 'Endereço',
    'addressPH': 'Pro Táxi ;)',
        
    'saveProfile': 'Salvar Perfil',
    'profileUpdated': 'Perfil atualizado. Aguarde os Uploads',
    
    'uploadingFiles': 'Enviando Arquivos...',
    'videoSent': 'Vídeo Enviado',
    'photoSent': 'Foto Enviada',
    'posterSent': 'Banner Enviado',
    'audioSent': 'Áudio Enviado',
    
    'happyBirthday': 'Feliz Aniversário!',
    'watchVideo': 'Assistir Vídeo',
    'tickets': 'Ingressos',
    'reservations': 'Reservas VIP',
    'tables': 'Reserva de Mesa',
    'reservation': 'Reserva',
    'website': 'Site Oficial',
    'listNames': 'Nomes na Lista',
    'listAdd': 'Adicionar-me à Lista',
    'listRemove': 'Remover-me da Lista',
    'listTimeout': 'Lista pode ser modificada até ',
    'emptyList': 'A lista está vazia, que tal ser o primeiro?',
    'clickButton': 'Pressione o botão',
    
    'wonderful': 'Marvilha',
    'noWorries': 'Tranquilo',
    
    'planning': 'Planejamento',
    'configurations': 'Configurações',
    'vipAreas': 'Áreas VIP',
    
    'table': 'Mesa',
    'tables': 'Mesas',
    'tableAdd': 'Adicionar mesa',
    'tablesEdit': 'Editar Mesas',
    'tablesReservations': 'Mesas Reservadas',
    
    'nextDate': 'Próximo Evento',
    'moreInfo': 'Mais Informações',
    'mapContact': 'Mapa & Contato',
    'techSupport': 'Suporte Técnico',
    
    'poweredBy': 'Produzido por',
    
    'support': 'Por favor entre em contato com o desenvolvedor do aplicativo, ele te ajudará em qualquer dificuldade',
       
    'datesAdmin': 'Edição de Eventos',
    'dateEdit': 'Edição de Evento',
    'datesList': 'Datas Agendadas',
    'datesNew': 'Criar novo Evento',
    
    'eventDate': 'Data do Evento',
    'eventTime': 'Hora do Evento',
    'eventName': 'Nome do Evento',
    'eventNamePH': 'Nome',
    'eventBanner': 'Enviar Banner',
    'eventVideo': 'Enviar Vídeo',
    'eventAudio': 'Enviar Áudio',
    'eventWebsite': 'URL Site Oficial',
    'eventWebsitePH': 'https://www...',
    'eventTickets': 'URL Venda de Ingressos',
    'eventTicketsPH': 'Pagseguro etc..',
    'eventRelease': 'Release',
    
    'delete': 'Apagar',
    'save': 'Salvar',
    
    'confirmIrreversible': "Confirmar ação irreversível: ",
    
    'reserved': 'Reservado',
    'confirmed': 'Confirmado',
    
    'reservationTimeLimit': "Horário máximo para reservas no dia",
    
    'lat': 'Latitude',
    'lng': 'Longitude',
    
    'happyBirthDayHelp': "Mensagem para mostrar ao usuário em seu aniversário",
    
    'vipMap': "Mapa camarotes VIP",
    'vipMapSent': "Mapa VIP enviado",
    'clubUpdated': "Clube atualizado",
    
    'vipEdit': "Edição área VIP",
    
    'price': "Preço"
    
  },
  
  'en':
  {
    
    'connecting': 'Connecting...',
    'start': 'Start',
    'loading': 'Loading...',
    'soSlow': 'Patience is Virtuous',
    'exclusiveAccess': 'Exclusive Access',
    'profileNetwork': 'Festaria Profile',
    'loginNetwork': 'Login on Festaria Network',
    'email': 'email',
    'password': 'password',
    'passwordConfirmation': 'password confirmation',
    'newAccount': 'New Account',
    'logIn': 'Login',
    'logOut': 'Logout',
    'disclaimer': 'By proceeding  you agree with out',
    'tos': 'Terms of Service',
    'pp': 'Privacy Policy',
    'needHelp': 'Need Help?',
    'newUserNetwork': 'New user on Festaria Network',
    'cancel': 'Cancel',
    'register': 'Register',
    
    'publicInfo': 'Public Info',
    'privateInfo': 'Personal Info',
    
    'publicInfoDisclaimer': 'Accessible by all users on this app',
    'privateInfoDisclaimer': 'Data shared between you and the house owner',
    
    'name': 'Name',
    
    'displayName': 'Name / Nickname',
    'displayNamePH': "You can fool around here.",
    
    'bio': 'Your public message',
    'bioDefault ': 'New user here 😄',
    
    'updatePhotoURL': 'Update Photo',
    'updateVideoURL': 'Update Video',
    
    'max2MB': 'Max file size: 2MB',
    'max3MB': 'Max file size: 3MB',
    'max10MB': 'Max file size: 10MB',
    
    
    'realName': 'Real name',
    'realNamePH': "Don't fool around here",
    
    'doc': 'Document Nº',
    'docPH': 'Neither Here',
    
    'birthDate': 'Birth Date',
    
    'phone': 'Phone',
    'phonePH': 'Also important',
    
    'address': 'Address',
    'addressPH': 'For the taxi ;)',
        
    'saveProfile': 'Save Profile',
    'profileUpdated': 'Profile Updated. Wait for the uploads',
    
    'uploadingFiles': 'Sending Files...',
    'videoSent': 'Video Sent',
    'photoSent': 'Photo Sent',
    'posterSent': 'Banner Sent',
    'audioSent': 'Audio Sent',
    
    'happyBirthday': 'Happy Birthday!',
    'watchVideo': 'Watch Video',
    'tickets': 'Tickets',
    'reservations': 'VIP Reservations',
    'tables': 'Table Reservation',
    'reservation': 'Reservation',
    'website': 'Official Site',
    'listNames': 'Names in the List',
    'listAdd': 'Add me to this List',
    'listRemove': 'Remove-me from List',
    'listTimeout': 'List can be changed until ',
    'emptyList': 'List is empty, how about being the first?',
    'clickButton': 'Tap the button',
    
    'wonderful': 'Wonderful',
    'noWorries': 'No Worries',
    
    'planning': 'Planning',
    'configurations': 'Configurations',
    'vipAreas': 'VIP Areas',
    
    
    'table': 'Table',
    'tables': 'Tables',
    'tableAdd': 'Add Table',
    'tablesEdit': 'Edit Tables',
    'tablesReservations': 'Table Reservations',
    
    'nextDate': 'Next Event',
    'moreInfo': 'More Info',
    'mapContact': 'Map & Contact',
    'techSupport': 'Tech Support',
    
    'poweredBy': 'Powered By',
    
    'support': 'Please contact our developer, he will help you',
       
    'datesAdmin': 'Events Administration',
    'dateEdit': 'Event Editing',
    'datesList': 'Scheduled Dates',
    'datesNew': 'Create new Event',
    
    'eventDate': 'Event Date',
    'eventTime': 'Event Time',
    'eventName': 'Event Name',
    'eventNamePH': 'Name',
    'eventBanner': 'Upload Banner',
    'eventVideo': 'Upload Video',
    'eventAudio': 'Upload Audio',
    'eventWebsite': 'URL Official Site',
    'eventWebsitePH': 'https://www...',
    'eventTickets': 'URL Tickets',
    'eventTicketsPH': 'Paypal etc..',
    'eventRelease': 'Release',
    
    'delete': 'Delete',
    'save': 'Save',
    
    'confirmIrreversible': "Confirm irreversible action: ",
    
    'reserved': 'Reserved',
    'confirmed': 'Confirmed',
    
    'reservationTimeLimit': "Reservation Time limit",
    
    'lat': 'Latitude',
    'lng': 'Longitude',
    
    'happyBirthDayHelp': "Message to show user on his birthday",
    
    'vipMap': "VIP Map",
    'vipMapSent': "VIP Maps sent",
    'clubUpdated': "Club updated",
    
    'vipEdit': "VIP area Edit",
    
    'price': "Price"
    
  }
  
}

