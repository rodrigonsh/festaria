function userProfilePic(p)
{
  let video = create('video')
  let img = create('img')
  let hasVideo = false
  let hasImg = false
  
  if ( !( 'photoURL' in p ) )
  {
    p.photoURL = "profile.mp4"
  }
  
  if ( 
    p.photoURL.indexOf('fbcdn.net') > -1 || 
    p.photoURL.indexOf('googleusercontent.com') > -1)
  {
    img.src = p.photoURL
    img.setAttribute('responsive')
    hasImg = true
  }
  
  else
  {
    video.src = p.photoURL
    video.autoplay = false
    video.controls = false
    video.loop = true
    video.setAttribute('emit', 'showControls')
    video.className = 'content'
    hasVideo = true
  }
  
  if (hasVideo) return video
  else return img
  
}
